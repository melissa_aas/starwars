package StarWars;

public class Gentil extends Personnage{

	private boolean force;
	
	public Gentil(String nom,String prenom, boolean f) {
		super(nom,prenom);
		this.force = f;
	}
	
	public boolean getForce() {
		return this.force;
	}
	public void setForce(boolean f) {
		this.force = f;
	}
	
	public String toString() {
		return super.toString()+"Force : "+this.force+"";
	}
}
