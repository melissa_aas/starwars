package StarWars;

public class Personnage {
    
	    private String nom;
		private String prenom;
		
		public Personnage() {}
		
		public Personnage(String n, String p) {
			this.nom = n;
			this.prenom = p;
		}
		
		public String getNom() {
			return this.nom;
		}
		public void setNom(String n) {
			this.nom = n;
		}
		
		public String getPrenom() {
			return this.prenom;
		}
		public void setPrenom(String p) {
			this.prenom = p;
		}
		
		public String toString() {
			return "Nom : "+this.nom+"\nPrenom :"+this.prenom+"";
		}

}
