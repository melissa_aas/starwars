package StarWars;

import java.util.ArrayList;
import java.util.Map;

public class Film {
	
    private String titre;
    private int anneeSortie;
    private int numeroEpisode;
    private double cout;
    private double recette;
    private ArrayList<Acteur> Acteur; //7eme question 
    
    public Film(String titre,int annee,int numero,double cout,double recette,ArrayList<Acteur> act) {
    	this.titre = titre;
    	this.anneeSortie = annee;
    	this.numeroEpisode = numero;
    	this.cout = cout;
    	this.recette = recette;
    	this.Acteur = act;
    }
    
    public Film() {}
    
    public String getTitre() {
    	return this.titre;
    }
    public void getTitre(String t) {
    	this.titre = t;
    }
    
    
    public int getAnnee() {
    	return this.anneeSortie;
    }
    public void setAnnee(int a) {
    	this.anneeSortie = a;
    }
    
    
    public int getNumero() {
    	return this.numeroEpisode;
    }
    public void setNumero(int n) {
    	this.numeroEpisode = n;
    }
    
    
    public double getCout() {
    	return this.cout;
    }
    public void setCout(double c) {
    	this.cout = c;
    }
    
    
    public double getRecette() {
    	return this.recette;
    }
    public void setRecette(double r) {
    	this.recette = r;
    }
    
    public ArrayList<Acteur> getActeur() {
    	return this.Acteur;
    }
    public void getActeur(ArrayList<Acteur> act) {
    	this.Acteur = act;
    }
    
    public String toString() {
    	return "Titre :"+this.titre+"\nAnnée de Sortie :"+this.anneeSortie+"\nNumero d'episode :"+this.anneeSortie+"\nCout :"+this.cout+"\nRecette :"+this.recette+"\nListe d'acteur :"+this.Acteur+"";
    }
    
    //5eme question 
    public static void afficheCollection(ArrayList<Object> collection) {
		for(Object obj : collection ) {
			System.out.println(obj.toString());
		}
	}
    
    
    //11eme question : TOUTES LES METHODES
    public int nbActeurs() {
    	return this.Acteur.size();
    }
    
    public int nbPersonnages() {
    	int nbperso = 0;
    	for(int i = 0; i< this.getActeur().size();i++) {
    		nbperso += this.getActeur().get(i).getPersonnage().size();
    	}
    	return nbperso;
    }
    
    public boolean isBefore(int annee) {
    	boolean result;
    	if (anneeSortie < annee) {
    		result = true;
    	}else {
    		result = false;
    	}
    	
    	return result;
    }
    
    public ArrayList<Object> calculeBenifice() {
    	ArrayList<Object> benef = new ArrayList<>();
   
    	boolean ft = false;
    	if((recette - cout) >= 0) {
    		ft = true;
    		benef.add(ft);
    		benef.add((recette - cout));
    	}else {
    		benef.add(recette - cout );
    		benef.add(ft);
    	}
    	return benef;
    }
    
    //13eme question
    public static void makeBackUp(Map<Integer,	Film> dictFilm) {
    	
    	for (Map.Entry<Integer, Film> entry : dictFilm.entrySet()) {
            System.out.println("Clé : " + entry.getKey() + ", Valeur : " + entry.getValue().titre+ "benefice"+ entry.getValue().calculeBenifice().toString());
        }
    	
    }
    
    //12eme question
    
}
