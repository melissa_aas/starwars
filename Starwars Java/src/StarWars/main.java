package StarWars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
//import java.util.Scanner;

public class main {
	
	public static void main(String[] args) {
		//1er film
		ArrayList<Personnage> perso1 = new ArrayList<Personnage>();
		Acteur acteur1 = new Acteur("Le M","Noir",perso1);
		Acteur acteur2 = new Acteur("Blanc","Antoine",perso1);
		Acteur acteur3 = new Acteur("Michel","Schofield",perso1);
		ArrayList<Acteur> a1 = new ArrayList<Acteur>();
		a1.add(acteur1);
		a1.add(acteur2);
		a1.add(acteur3);
		
		Film f1 = new Film("Un nouvel espoir",1977,4,184.234,12345,a1);
		Film f2 = new Film("Menace Fantôme",1977,4,184.234,12345,a1);
	//System.out.println("Film 1 :\n"+f1.toString());
		//f1.add(a1);
		
		//2eme film qui sera créer par l'utilisateur
		//Scanner scanner = new Scanner(System.in);
		//Film f2 = new Film(scanner.nextLine(),scanner.nextInt(),scanner.nextInt(),scanner.nextDouble(),scanner.nextLine(),);
		
		//3eme question :
		Personnage p1 = new Personnage("Melissa","PLB");
		Personnage p2 = new Personnage("M&M","Bon");
		//System.out.println("\n3eme question\n"+p1.toString());
		
		//8eme question
		perso1.add(p1);
		perso1.add(p2);
		
		//4eme question :
		ArrayList<Object> listObjet = new ArrayList<Object>();
		listObjet.add(f1);
		listObjet.add(p1);
		//System.out.println("\n4eme question\n"+listObjet);
		
		//6eme question :
		Film.afficheCollection(listObjet);
		System.out.println(f1.calculeBenifice().toString());
		
		//13eme question 
		Map<Integer,Film> dic = new HashMap<>();
		dic.put(1990, f1);
		dic.put(2000,f2);
		Film.makeBackUp(dic);
		
		
	}

}
