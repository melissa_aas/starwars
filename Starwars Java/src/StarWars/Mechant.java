package StarWars;


public class Mechant extends Personnage{
   
	private boolean coteObscur;
	
	public Mechant(String nom,String prenom,boolean c) {
		super(nom,prenom);
		this.coteObscur = c;
	}
	
	public boolean getCoteobscur() {
		return this.coteObscur;
	}
	public void setCoteObscur(boolean c) {
		this.coteObscur = c;
	}
	
	public String toString() {
		return super.toString()+"Cote obscur est : "+this.coteObscur+"";
	}
}
