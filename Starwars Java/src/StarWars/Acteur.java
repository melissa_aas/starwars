package StarWars;

import java.util.ArrayList;

public class Acteur {

	private String nom;
	private String prenom;
	private ArrayList<Personnage> Personnage;//7eme question
	
	public Acteur() {}
	
	public Acteur(String n, String p,ArrayList<Personnage> per) {
		this.nom = n;
		this.prenom = p;
		this.Personnage = per;
	}
	
	public String getNom() {
		return this.nom;
	}
	public void setNom(String n) {
		this.nom = n;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	public void setPrenom(String p) {
		this.prenom = p;
	}
	
	public ArrayList<Personnage> getPersonnage() {
		return this.Personnage;
	}
	public void setPersonnage(ArrayList<Personnage> per) {
		this.Personnage = per;
	}
	
	public String toString() {
		return "Nom : "+this.nom+"\nPrenom :"+this.prenom+"\nListe des personnages"+this.Personnage+"";
	}
	
	
	//9eme question
	int nbPersonnage() {
		return this.Personnage.size();
	}
	
	
	
	
	
	
}
