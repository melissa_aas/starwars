package test;


public class Visite {
    String nomEtudiant;
    String date;
    int note;
    
    public Visite() {} //par défaut
    
    public Visite(String nomEtu, String date, int note) { //3 paramètres
    	this.nomEtudiant = nomEtu;
    	this.date = date;
    	this.note = note;
    }
    
    String getNom() {
    	return this.nomEtudiant;
    }
    void setNom(String nom) {
    	this.nomEtudiant = nom;
    }
    
    
    String getDate() {
    	return this.date;
    }
    void setDate(String d) {
    	this.date = d;
    }
    
    
    int getNote() {
    	return this.note;
    }
    
    void setNote(int n) {
    	this.note = n;
    }
    
    public String toString() {
    	return "nom Etudiant = "+this.nomEtudiant+" , \ndate = "+this.date+", \nNote = "+this.note+"";
    }
    
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Visite v1 = new Visite();
        Visite v2 = new Visite("MELISSA","20-12-2023",14);
        // System.out.println(v2.nomEtudiant);
        System.out.println(v2.toString());
	}

}

