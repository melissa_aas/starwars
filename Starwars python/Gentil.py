class Personnage:
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}"


# Example obj
personnage = Personnage(nom="Luke", prenom="Skywalker")
print(personnage)

class Gentil(Personnage):
    def __init__(self, nom="", prenom="", force=False):
        super().__init__(nom, prenom)
        self.force = force

    def get_force(self):
        return self.force

    def set_force(self, force):
        self.force = force

    def __str__(self):
        return f"{super().__str__()}Force: {self.force}"




class Personnage:
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}"



gentil = Gentil(nom="Luke", prenom="Skywalker", force=True)
print(gentil)
