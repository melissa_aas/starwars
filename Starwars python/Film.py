class Film:
    def __init__(self, titre="", annee=0, numero=0, cout=0.0, recette=0.0, acteurs=None):
        self.titre = titre
        self.anneeSortie = annee
        self.numeroEpisode = numero
        self.cout = cout
        self.recette = recette
        self.acteurs = acteurs if acteurs is not None else []

    def get_titre(self):
        return self.titre

    def set_titre(self, titre):
        self.titre = titre

    def get_annee(self):
        return self.anneeSortie

    def set_annee(self, annee):
        self.anneeSortie = annee

    def get_numero(self):
        return self.numeroEpisode

    def set_numero(self, numero):
        self.numeroEpisode = numero

    def get_cout(self):
        return self.cout

    def set_cout(self, cout):
        self.cout = cout

    def get_recette(self):
        return self.recette

    def set_recette(self, recette):
        self.recette = recette

    def get_acteurs(self):
        return self.acteurs

    def set_acteurs(self, acteurs):
        self.acteurs = acteurs

    def __str__(self):
        return f"Titre: {self.titre}\nAnnée de Sortie: {self.anneeSortie}\nNumero d'episode: {self.numeroEpisode}\nCout: {self.cout}\nRecette: {self.recette}\nListe d'acteur: {self.acteurs}"

    @staticmethod
    def affiche_collection(collection):
        for obj in collection:
            print(obj)

    def nb_acteurs(self):
        return len(self.acteurs)

    def nb_personnages(self):
        nb_perso = 0
        for acteur in self.acteurs:
            nb_perso += len(acteur.get_personnages())
        return nb_perso

    def is_before(self, annee):
        return self.anneeSortie < annee

    def calcule_benefice(self):
        benef = []
        ft = False
        if self.recette - self.cout >= 0:
            ft = True
            benef.append(ft)
            benef.append(self.recette - self.cout)
        else:
            benef.append(self.recette - self.cout)
            benef.append(ft)
        return benef

    @staticmethod
    def make_backup(dict_film):
        for key, value in dict_film.items():
            print(f"Clé: {key}, Valeur: {value.titre}, bénéfice: {value.calcule_benefice()}")


film = Film(titre="Star Wars", annee=1977, numero=4, cout=11.0, recette=775.4, acteurs=[])


print(film)


print(f"Number of actors: {film.nb_acteurs()}")


print(f"Number of characters: {film.nb_personnages()}")


print(f"Is before 2000: {film.is_before(2000)}")


print(f"Profit or Loss: {film.calcule_benefice()}")


films_dict = {1: film}
Film.make_backup(films_dict)
