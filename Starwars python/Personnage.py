class Personnage:
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}"



personnage = Personnage(nom="Luke", prenom="Skywalker")
print(personnage)
