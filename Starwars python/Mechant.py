class Personnage:
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}"


# Example obj
personnage = Personnage(nom="Luke", prenom="Skywalker")
print(personnage)

class Mechant(Personnage):
    def __init__(self, nom="", prenom="", cote_obscur=False):
        super().__init__(nom, prenom)
        self.cote_obscur = cote_obscur

    def get_cote_obscur(self):
        return self.cote_obscur

    def set_cote_obscur(self, cote_obscur):
        self.cote_obscur = cote_obscur

    def __str__(self):
        return f"{super().__str__()}Cote obscur est : {self.cote_obscur}"




class Personnage:
    def __init__(self, nom="", prenom=""):
        self.nom = nom
        self.prenom = prenom

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}"



mechant = Mechant(nom="Darth", prenom="Vader", cote_obscur=True)
print(mechant)
