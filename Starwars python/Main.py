import Acteur
import Personnage
import Film

#class Personnage:
   # def __init__(self, nom="", prenom=""):
    #    self.nom = nom
     #   self.prenom = prenom

    #def __str__(self):
       # return f"Nom: {self.nom}\nPrenom: {self.prenom}"
#class Acteur:
    #def __init__(self, nom="", prenom="", personnages=None):
       # self.nom = nom
       # self.prenom = prenom
       # self.personnages = personnages if personnages is not None else []

   # def __str__(self):
 #       return f"Nom: {self.nom}\nPrenom: {self.prenom}\nListe des personnages: {self.personnages}"

perso1 = [Personnage("Melissa", "PLB"), Personnage("M&M", "Bon")]

acteur1 = Acteur("Le M", "Noir", perso1)
acteur2 = Acteur("Blanc", "Antoine", perso1)
acteur3 = Acteur("Michel", "Schofield", perso1)
a1 = [acteur1, acteur2, acteur3]

f1 = Film("Un nouvel espoir", 1977, 4, 184.234, 12345, a1)
f2 = Film("Menace Fantôme", 1977, 4, 184.234, 12345, a1)

p1 = Personnage("Melissa", "PLB")
p2 = Personnage("M&M", "Bon")

perso1.extend([p1, p2])

list_objet = [f1, p1]

Film.affiche_collection(list_objet)
print(f1.calcule_benefice())

dic = {1990: f1, 2000: f2}
Film.make_backup(dic)
