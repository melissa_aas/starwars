class Acteur:
    def __init__(self, nom="", prenom="", personnages=None):
        self.nom = nom
        self.prenom = prenom
        self.personnages = personnages if personnages is not None else []

    def get_nom(self):
        return self.nom

    def set_nom(self, nom):
        self.nom = nom

    def get_prenom(self):
        return self.prenom

    def set_prenom(self, prenom):
        self.prenom = prenom

    def get_personnages(self):
        return self.personnages

    def set_personnages(self, personnages):
        self.personnages = personnages

    def __str__(self):
        return f"Nom: {self.nom}\nPrenom: {self.prenom}\nListe des personnages: {self.personnages}"

    def nb_personnages(self):
        return len(self.personnages)




actor = Acteur(nom="Doe", prenom="John", personnages=["Character1", "Character2"])


print(actor)


print(f"Number of characters: {actor.nb_personnages()}")
